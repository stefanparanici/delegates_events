﻿using System;
using System.Collections.Generic;

namespace delegates_events
{
    class PersList : List<Person>
    {
        public Action<Person> AddPersons;

        public delegate void EventDelegate();

        public event EventDelegate ONfiveEvent;
        public EventDelegate ShowPersons;

        public PersList()
        {
            AddPersons = Add;
            AddPersons += elemnt =>
            {
                if(!elemnt.Name.Contains('a'))
                    this.Remove(elemnt);
            };
            AddPersons += elemnt =>
            {
                if (elemnt.Age < 18)
                    this.Remove(elemnt);
            };
            ShowPersons = ShowList;
            ShowPersons += () =>
            {
                if (this.Count >= 5)
                    if (ONfiveEvent != null)
                        ONfiveEvent();
            };

            ONfiveEvent += () =>
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("More than 5 persons,YEY :)");
                Console.ForegroundColor = ConsoleColor.White;
            };
        }

        private void ShowList()
        {
            if (this.Count == 0)
            {
                Console.WriteLine("No Persons Here");
                return;
            }

            string name = "Name", age = "Age";
            Console.WriteLine(name.PadRight(10, ' ') + age.PadLeft(3, ' '));
            foreach (var elemnt in this)
            {
                name = elemnt.Name;
                age = elemnt.Age.ToString();
                Console.WriteLine(name.PadRight(10, ' ') + age.PadLeft(3, ' '));
            }
        }
    }
}
