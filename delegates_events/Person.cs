﻿using System;

namespace delegates_events
{
    class Person
    {
        public Person()
        {
            Console.Write("Person's name:");
            Name = Console.ReadLine();
            Console.Write("Person's age:");
            Age = Convert.ToInt32(Console.ReadLine());
        }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
