﻿using System;

namespace delegates_events
{
    class Program
    {
        static void Main()
        {
            char optiune;
            PersList personsList = new PersList();
            while (true)
            {
                Console.Clear();
                personsList.ShowPersons();
                Console.WriteLine("!!!Press enter to introduce a new person!!!!"); 
                Console.WriteLine("          Press X to Close the app          ");
                optiune = Console.ReadKey().KeyChar;
                switch (Char.ToUpper(optiune))
                {
                    case '\r':
                        personsList.AddPersons(new Person());
                        break;
                    case 'X':
                        return;
                    default:
                        Console.WriteLine("Wrong answer :)");
                        Console.ReadKey();
                        break;
                }
            }
        }
    }
}
